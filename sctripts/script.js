const mainQuerry = function(){
    fetch('https://www.reddit.com/r/aww/hot.json')
    .then(response => response.json())
    .then(response => {
        $('.loader-wrapper').hide()
        $('.item-list').show()
        console.log(response)

        let arrayItems = response.data.children
        console.log(arrayItems)
        arrayItems.map(function(item){
            $('.item-list').append(`<div class="item"><div class="title"><h2>${item.data.title}</h2><span>by ${item.data.author}</span></div><div class="content"><img class="img-preview" src="${item.data.preview.images[0].source.url}" alt="${item.data.title}"></div></div>`)
        })

    })
}

$(function() {
    mainQuerry()
})

